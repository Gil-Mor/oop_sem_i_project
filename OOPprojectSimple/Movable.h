#pragma once
#include "GameObject.h"

class Movable : public GameObject
{
public:
    Movable();
    virtual ~Movable()=0;

    enum Direction_E
    {
        UP_D,
        DOWN_D,
        LEFT_D,
        RIGHT_D
    };

    virtual void colide(GameObject& other) = 0;
    virtual void colide(King& other) = 0;
    virtual void colide(Mage& other) = 0;
    virtual void colide(Thief& other) = 0;
    virtual void colide(Warrior& other) = 0;
};

