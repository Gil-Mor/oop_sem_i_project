#pragma once
#include "MusicButton.h"

class VolumeButton :
    public MusicButton
{
public:

    VolumeButton(const sf::Vector2f& size, 
        const sf::Vector2f& pos,
        sf::Music& soundTrack, bool whichWay);

    ~VolumeButton();

    virtual void action() const;

private:

    /* Tells if to lower the volume or increase it,
    if true then the button increases the volume,
    If false it decreases it.*/
    bool _up;
};

