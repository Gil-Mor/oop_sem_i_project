#include "Fire.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Fire::Fire()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::FIRE_T));
}


Fire::~Fire()
{
}

void Fire::colide(GameObject& other)
{
    other.colide(*this);
}

void Fire::colide(Mage& other)
{
    other.colide(*this);
}

void Fire::colide(King& other)
{
    other.colide(*this);
}

void Fire::colide(Thief& thief)
{
    thief.colide(*this);
}

void Fire::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
