#pragma once
#include "GameObject.h"
class Gate :
    public GameObject
{
public:
    Gate(bool isOpen = false);
    ~Gate();

    bool isOpen() const;
    void setOpen(bool o);

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);

private:
    bool _isOpen;
};

