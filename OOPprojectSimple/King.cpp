#include "King.h"

#include "BlankTile.h"
#include "Throne.h"
#include "Gate.h"
#include "Key.h"
#include "Teleport.h"


King::King()
    :_onThrone(false)
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::KING_T));


}


King::~King()
{
}

bool King::onThrone() const
{
    return _onThrone;
}

void King::colide(GameObject& other)
{
    other.colide(*this);
}


void King::colide(King& other)
{
    return;
}

void King::colide(Mage& other)
{
    return;
}
void King::colide(Thief& other)
{
    return;
}
void King::colide(Warrior& other)
{
    return;
}
void King::colide(BlankTile& other)
{
    moveToPosition(other.getBoardPosition());

}
void King::colide(Wall& other)
{
    return;
}
void King::colide(Fire& other)
{
    return;
}
void King::colide(Throne& other)
{
    moveToPosition(other.getBoardPosition());
    _onThrone = true;

}

void King::colide(Gate& other)
{
    if (other.isOpen())
        moveToPosition(other.getBoardPosition());
}

void King::colide(Key& other)
{
    moveToPosition(other.getBoardPosition());
}

void King::colide(Orc& other)
{
    return;
}

void King::colide(Teleport& other)
{
    GameObject* atTwin = _commands->getFromBoard(other.getTwinPosition());
    if (typeid(*atTwin) != typeid(other))
        return;

    moveToPosition(other.getTwinPosition());
}