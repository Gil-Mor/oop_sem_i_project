#include "Warrior.h"
#include "BlankTile.h"
#include "Gate.h"
#include "Key.h"
#include "Orc.h"
#include "Teleport.h"
#include "CharacterBoardCommand.h"
Warrior::Warrior()
    :_bodyCount(0), _health(100)
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::Objects_E::WARRIOR_T));
}


Warrior::~Warrior()
{
}


void Warrior::colide(GameObject& other)
{
    other.colide(*this);
}

void Warrior::colide(Warrior& other)
{
    return;
}


void Warrior::colide(King& other)
{
    return;
}

void Warrior::colide(Mage& other)
{
    return;
}

void Warrior::colide(Thief& other)
{
    return;
}

void Warrior::colide(BlankTile& other)
{
    moveToPosition(other.getBoardPosition());
}

void Warrior::colide(Wall& other)
{
    return;
}

void Warrior::colide(Fire& other)
{
    return;
}

void Warrior::colide(Throne& other)
{
    return;
}

void Warrior::colide(Gate& gate)
{
    if (gate.isOpen())
        moveToPosition(gate.getBoardPosition());
    return;
}

void Warrior::colide(Key& key)
{
    moveToPosition(key.getBoardPosition());
}

void Warrior::colide(Orc& orc)
{
    kill(orc);
    moveToPosition(orc.getBoardPosition());
}

void Warrior::colide(Teleport& teleport)
{
    GameObject* atTwin = _commands->getFromBoard(teleport.getTwinPosition());
    if (typeid(*atTwin) != typeid(teleport))
        return;

    moveToPosition(teleport.getTwinPosition());
}

void Warrior::kill(Orc& orc)
{
    if (_commands->thiefHasKey())
    {
        _commands->setOnBoard(new BlankTile, orc.getBoardPosition());
    }
    else
    {
        _commands->setOnBoard(new Key, orc.getBoardPosition());
    }
}