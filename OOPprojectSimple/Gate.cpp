#include "Gate.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Gate::Gate(bool isOpen /* = false */)
    :_isOpen(isOpen)
{
    if (isOpen)
    {
        _shape.setTexture(
            TextureManager::getInstance()->getObjectsTex(Objects_Tex::OPEN_GATE_T));
    }
    else
    {
        _shape.setTexture(
            TextureManager::getInstance()->getObjectsTex(Objects_Tex::CLOSED_GATE_T));
    }

}


Gate::~Gate()
{
}

bool Gate::isOpen() const
{
    return _isOpen;
}

void Gate::setOpen(bool o)
{
    _isOpen = o;


    if (o)
    {
        _shape.setTexture(
            TextureManager::getInstance()->getObjectsTex(Objects_Tex::OPEN_GATE_T));
    }
}


void Gate::colide(GameObject& other)
{
    other.colide(*this);
}
void Gate::colide(Mage& other)
{
    other.colide(*this);
}

void Gate::colide(King& other)
{
    other.colide(*this);
}

void Gate::colide(Thief& thief)
{
    thief.colide(*this);
}

void Gate::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
