#pragma once

/* Loads levels from .stk files. 
Holds the info from the file in LevelStruct which it
passes to the controller.
*/
#include <fstream>
#include <vector>
#include <string>
#include "TextureManager.h"

using std::string;
using std::vector;
using std::ifstream;

class GameObject;
class Character;
class ObjectManager;

class LevelBuilder
{
public:

    LevelBuilder();
    ~LevelBuilder();

    // Level Info
    struct LevelStruct
    {
        // the board with game objects.
        vector < vector<GameObject*> > board;

        // the moving characters come seperatly.
        vector<Character* > characters;

        // levelSize
        sf::Vector2u size;

        // level time limit
        int time;

        // level moves limit
        int moves;
    };

    // GameObject info.
    // keeps the information on an object 
    // in creation.
    struct ObjectInfo
    {
        // object name. can be used to save to objects to
        // a file.
        string name;

        // the objects color.
        sf::Color color;

        // true if there's a specified color for the object.
        bool hasColor;

        // true if the object is large (wall or fire)
        bool large;

        // all the objects potiions. 
        // only one position in case of a normal object.
        // more in case of a large object.
        // more flexability using vector and 
        // merging two cases.
        vector<sf::Vector2u> allPositions;
    };

    /* loads level 'level' from levels */
    LevelBuilder::LevelStruct& loadLevel(const unsigned int level);

    /* return the number of levels. */
    unsigned int numOfLevels() const;

    // ============ PRIVATE ==================
private:

    ObjectManager* _objectsCreator;
    LevelStruct _level;
    ifstream _inputLevels;

    vector<string> _levelsFiles;
    void LevelBuilder::storeLevelFileNames();
    string getObjectName(const string line) const;

    void loadLevel(ifstream& file, LevelStruct& level);

    void LevelBuilder::buildObject(
        const string line,
        LevelStruct& level);

    void createObject(LevelStruct& level, const ObjectInfo& objInfo);

    vector<unsigned int> LevelBuilder::getValuesAfter(
        const string str,
        const string substr,
        const unsigned int howManyNums) const;

    vector<sf::Vector2u> getAllCoordinates(const string& line,
        const sf::Vector2u head) const;


    vector<unsigned int> getNumbers(const string& line,
        int howMany, const size_t pos) const;


    void getSizeTimeMoves(ifstream& file, LevelStruct& level);


    void getObjectDirection(const string& line,
        int& rowDir, int& colDir) const;

    sf::Vector2u to2U(const vector<unsigned int>& v) const;

    sf::Color toRGBA(const vector<unsigned int> v) const;

    void LevelBuilder::createLarge(
        LevelStruct& level,
        const ObjectInfo& object);

    void createSingle(
        LevelStruct& level,
        const ObjectInfo& obj,
        const sf::Vector2u position);

    ObjectInfo initObjectStruct() const;

    void resizeLevelVector(LevelStruct& level);

};

