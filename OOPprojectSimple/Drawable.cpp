#include "Drawable.h"



mydrawable::Drawable::Drawable()
{
    _shape.setFillColor(DEFAULT_COLOR);
}

mydrawable::Drawable::Drawable(const sf::RectangleShape& shape)
{
    _shape = shape;

    // if the shape has no previous assigned color assign the default color.
    if (shape.getFillColor() == sf::Color::Transparent)
        _shape.setFillColor(DEFAULT_COLOR);
}


mydrawable::Drawable::~Drawable()
{
}


void mydrawable::Drawable::show(sf::RenderWindow& window) const
{
    window.draw(_shape);
}

const sf::RectangleShape& mydrawable::Drawable::getShape() const
{
    return _shape;
}


