#include "GameObject.h"


GameObject::GameObject()
{
    _shape.setSize(OBJECT_SIZE);
}


GameObject::~GameObject()
{
}

sf::Vector2u GameObject::getBoardPosition() const
{
    return _position;
}

void GameObject::setBoardPosition(const sf::Vector2u pos)
{
    _position = pos;
}

sf::Vector2f GameObject::getShapePosition() const
{
    return _shape.getPosition();
}

void GameObject::setShapePosition(const sf::Vector2f pos)
{
    _shape.setPosition(pos);
}

void GameObject::setColor(const sf::Color& color)
{
    _shape.setFillColor(color);
}

string GameObject::getName() const
{
    return _name;
}

void GameObject::setName(const string& name)
{
    _name = name;
}

const sf::Texture* GameObject::getTexture() const
{
    return _shape.getTexture();
}
