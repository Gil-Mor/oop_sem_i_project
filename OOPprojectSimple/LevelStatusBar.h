#pragma once
#include "Drawable.h"
#include "string"

using std::string;

class GameObject;

class LevelStatusBar : public mydrawable::Drawable
{
public:

    LevelStatusBar();
    ~LevelStatusBar();

    void create(
        const sf::Vector2f size,
        const sf::Vector2f pos,
        bool timeLimit,
        bool movesLimit);

    /* Set the Texture of the current characte image*/
    //void changeCurrCharacter(const GameObject* currCharacter);

    void show(
        sf::RenderWindow& window, 
        const GameObject* currCharacter,
        float time, unsigned int steps);


private:

    sf::RectangleShape _currCharacterImage;
    sf::Font _font;
    sf::Text _time;
    sf::Text _moves;

    string _timePrefix, _movesPrefix;
    
};

