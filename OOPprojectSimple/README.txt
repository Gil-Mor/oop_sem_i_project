﻿~~~~~~~ README FOR: Ex5 OOP Project ~~~~~~

2. -------------- STUDENT --------------- 
Gil Mor גיל מור 300581030

3.------------- THE EXERCISE ---------------------



4.$$$$$$$$$$$$$$$$$$$$$ DESIGN $$$$$$$$$$$$$$$$$$$$$$

+++++++ for headLines

==== 'SomeName' ==== for classes documentation

*** for explenations


++++++++++++++ FLOW CONTROL +++++++++++++++++++++++

================ Controller ================
Controls the flow of the program.
Channels control to different parts of the program
like the Menu, Level(game), messages to the user etc..

**** The controller is 'State based'. ***
it operates according to a global state
(an enum of states).

** Different parts of the program can change the global state
via 'ControllerCommands' classes that encapsultes calls
to set and get State from the controller.
The class that encapsulate this commands is 'StateCommnads'
which is derived from 'Controller Commands'.
This is a sort of variance on "Command pattern"..
Now, if i want to give classes the ability to change the state of
the program i only need to give them the instance of StateCommands.
If I need more abilitys I need to make more classes or decide that
i'm merging more funcinality into one ControllerCommands class.

** The Menu and Level (the game.. current Level) are both runnning
an SFML loop while the global state says that we're in the menu or
we're playing. Both the Menu and Level are getting their input themselves.
Both of them canchange the state and by that 'exit' from themselves.
For example pressing Escape while playing changes the state to 'showing_menu'
and then we leave the Level loop andthe Controller channels us to the menu.




++++++++++++++++++ OBJECT CREATION +++++++++++++++++++++

=============== ObjectManager ==================
Singleton that Manages the creation of objects for each level.
Collects Characters and keeps them out of the board.
Sets the teleports in pairs.

*** Singleton
I don't know if it has to be a singleton..
If it win't then there's a chance for problems..
if two different of characters are created, 
The two of them need to be cleaned for the nextlevel loading..
could be a problem..

*** ObjectManager
I read somewhere that there might be a time when
you'll need to have more control over how the game objects
get created but you don't want to give Levelbuilder
that responsabilty. So let ObjectManager handle the creation and 
managment of objects.

*** LevelBUilder and ObjectManager
So during level loading LevelBuilder asks for GameObjects
from ObjectManager. 
Object Manager collects Characters and teleports during this proccess.
The Teleports he just collects to assign their 'twins'
and they end up as a part of the board.
but Characters are not part of the board so
he collects them seperatly and in the end he gives them
to LevelBuilder.




++++++++++++++++++++++ THE GAME +++++++++++++++++++++++++

The game is impleneted as a matrix of GameObject* (The board).
A 2d tile map.

Each object has a board Position and an sfml position correspanding to that
position.

The Characters are not a part a of the board. This eliminates
the need to take care of saving whats under the charactersas they move, 
somthing that get tricky..

Similar to ControllerCommands, The Characters interact with the board
through a 'CharacterBoardCommands' which is derived from 'BoardCommands'.
it alsostores a reference to the board and calls to certain function
of the board like getting the object in a certain tile etc...


The board also holds the vector of characters (along with Level). 
The Charactersare asking the board whats in some position.
in  order for the board to know if there's a character there
and to return it it needs the characters. 

also, the board needs to give the characters sfml posiiton according
to their boardPosition. 

I could have done it so that the characters are
asking the Level for stuff and not the board but i think it's
only another layer which doesn't really gives anything.


================ TextureManager ================
Singleton incharge of loading and keeping
all the program textures. 

**** This insures that the textures will be loaded only once
and except that, a lot of stuff need access
to different textures all the time so this is convinient.

******* SYNCING files and enums ****
The files with texture file names need to be in sync with the enums
in TextureManager!




===== MouseInteractable =====
Class representing objects that interact wit the mouse.
These objects can check if the mouse is hovering above
them and get to inFoscus mode.
Also they can can be set to chosen if the mouse clickedthem.
Buttons and GameObjects are derived from this class.

*** This should be an interface... I only need Character
class to inherit from this but Character inherits from GameObject
so i had to derive GameObjectfrom MouseInteractable giving
all game objects funcionality they're not going to use.


======= mydrawable::Drawable =======
Keeps a sf::RectangleShape for each class that needs it.
Every class that even needs to draw a background inherits from
this class. 

*** the member sf::RectangleShape is protected, giving each
instance of derived classes the option to get to it's own
shape.



+++++++++++++++++ MEMORY MANAGEMNET +++++++++++++++++++++
unfortunatly I didn't get to smart pointers.
the Controller needs to clean beforeloading a new level.
The Controller cleans the LevelStruct from LevleBuilder.









5. $$$$$$$$$$$$$$$$$$$ LIST OF FILES $$$$$$$$$$$$$$$$$$$$$$$$$$$

- mydrawable::Drawable 
Keeps a sf::RectangleShape for each class that needs it.
Every class that even needs to draw a background inherits from
this class. 

- LevelBuilder 
Loads levels from .stk files. 
Holds the info from the file in LevelStruct which it
passes to the controller.

- Level:
Incharge Of Running a single level in the game.
holds the board and a status bar.

- Controller Commands 
abstract Base Class.
Encapsulates calls to member functions of the controller
without exposing the entire controller interface.
different classes with different class can inherit.

- StateCommands 
Inherits from ControllerCommands.
Encapsulates calls to get and set state member functions
of the controller to get and set the global state of
the program. 

- Chronometer 
A header from "sftools" i'm using for a clock in Level.
Chronometer implements a pausable clock.
*** I found this when i was looking fora tutorial on
how to make a pausabel clock.

- Board
Matrix of GameObject* as the gameBoard.

- BoardCommand
abstract Base Class.
Encapsulate calls to member function of Board class giving 
only needed funcionality to objects that need it.
 
- Character:
A user controlable Character. 
Base Class of: King, Mage, Thief, and Warrior. 




6.--------------- DATA STRUCTURES -----------------

TextureManager and ObjectManagerare Singletons.

unordered_map to map strings of textures file names to enums.


7. --------------- NOTABLE ALGORITHMS -----------------
Double Dispatch.

8. ------------------- KNOWN BUGS ---------------------


-------- This is what saving the object's board position in the object itself
         gives me.

void Mage::colide(Fire& fire, vector<vector<GameObject*> >& board)
{
    putOutFire(board, fire);
    moveToPosition(board, fire);
}

