#include "Level.h"
#include "TextureManager.h"
#include "LevelBuilder.h"
#include "StateCommands.h"
#include "ObjectManager.h"

#include "Character.h"
#include "BlankTile.h"
#include "Wall.h"
#include "King.h"
#include <string>
#include <cmath>

using std::abs;
using std::string;


Level::Level(StateCommands& commands)
    :_stateCommands(commands),
    _characterCommnads(*(new CharacterBoardCommand(_board)))
{
}


void Level::create(const sf::RenderWindow& window)
{
    _shape.setSize(sf::Vector2f(window.getSize()));

    _shape.setPosition(sf::Vector2f(window.getPosition()));

    _shape.setTexture(TextureManager::getInstance()->
        getBoardTex(Board_Tex::BACKGROUND_T));
}

void Level::setNewLevel(LevelBuilder::LevelStruct& level)
{

    _characters = level.characters;

    // give commands to characters.
    giveCommands();

    // set the king to be the first character played.
    _currCharacter = ObjectManager::Characters_E::KING_C;

    _characters[_currCharacter]->setChosen(true);

    // save a pointer to the King for checking if he's on the throne.
    _king = dynamic_cast<King*>(_characters[_currCharacter]);


    _level = level;

    sf::Vector2f levelSize = _shape.getSize();
    sf::Vector2f boardSize = calcBoardSize();
    sf::Vector2f boardPos = calcBoardPos(boardSize);

    // board is created here because it's size is changing with every new level.
    _board.create(boardSize, boardPos);

    _statusBar.create({ levelSize.x * 1.f / 7.f, levelSize.y }, _shape.getPosition(),
        level.time > 0, level.moves > 0);

    initStatus();

    _view = { { levelSize.x / 2.f, levelSize.y / 2.f }, levelSize };

    _board.setBoard(level.board, _characters);

}


void Level::giveCommands()
{
    for (unsigned i = 0; i < _characters.size(); ++i)
    {
        _characters[i]->setCommands(&_characterCommnads);
    }
}

sf::Vector2f Level::calcBoardSize()
{
    // _boardSize is used for the view calculations.
    // + 1 for suroundoing walls..
    _boardSize = { (_level.size.x + 1 )*OBJECT_SIZE.x, (_level.size.y + 1) * OBJECT_SIZE.y };
    return _boardSize;
}

sf::Vector2f Level::calcBoardPos(const sf::Vector2f boardSize) const
{
    return{ (_shape.getSize().x - boardSize.x) / 2.f,
        (_shape.getSize().y - boardSize.y) / 2.f };
}


void Level::initStatus()
{
    _status.steps = 0;
    _status.time = 0;

    _status.limitMoves = _level.moves != 0;
    _status.limitTime = _level.time != 0;
    _status.clock.reset();
}



Level::~Level()
{
}

void Level::run(sf::RenderWindow& window)
{

    if (!_status.clock.isRunning())
    {
        _status.clock.resume();
    }

    sf::Vector2f mouse;
    while (_stateCommands.getState() == Controller::State_E::Playing_S)
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            // always convert pixels to window coordinates
            mouse = window.mapPixelToCoords(sf::Mouse::getPosition(window), _view);
            switch (event.type)
            {
                case sf::Event::KeyPressed:
                {
                    handleKeyBoard(event.key.code);
                    break;
                }
                // catch the resize events
                if (event.type == sf::Event::Resized)
                {
                    // update the view to the new size of the window
                    sf::FloatRect visible(0.f, 0.f, (float)event.size.width, (float)event.size.height);
                    window.setView(sf::View(visible));
                }
                case sf::Event::MouseMoved:
                {                  
                    handleMouseMove(mouse);
                    break;
                }
                case sf::Event::MouseButtonPressed:
                {
                    handleMouseClick(mouse);
                    break;
                }
                case sf::Event::Closed:
                    _stateCommands.setState(Controller::State_E::Exiting_S);
                    break;

                default:
                    break;
            }
        }

        checkStatus();

        window.clear();
        show(window);
        window.display();
    }

    // resore default view when leaving the level window.
    window.setView(window.getDefaultView());

}

void Level::handleKeyBoard(sf::Keyboard::Key key)
{
    if (key == sf::Keyboard::P)
    {
        _currCharacter = ObjectManager::Characters_E(
            (_currCharacter + 1) % _characters.size());
        chooseNewCharacter(_currCharacter);
    }
    else if (arrowKey(key))
    {
        if (_characters[_currCharacter]->move(key))
        {
            // solves the problem where the characters moves
            // when the mouse was hovering over it and it 
            // stayed in focus.
            _characters[_currCharacter]->setInFocus(false);
            _status.steps++;
        }
    }

    else if (key == sf::Keyboard::Escape)
    {
        _status.clock.pause();
        _stateCommands.setState(Controller::State_E::ShowingMenu_S);
    }
}

void Level::checkStatus()
{
    if (_status.limitTime)
    {
        if (_status.clock.getElapsedTime().asSeconds() >= _level.time)
        {
            _stateCommands.setState(Controller::State_E::Losing_S);
        }
    }

    if (_status.limitMoves)
    {
        if (_status.steps >= _level.moves)
        {
            _stateCommands.setState(Controller::State_E::Losing_S);
        }
    }

    if (_king->onThrone())
    {

        _stateCommands.setState(Controller::State_E::Winning_S);
    }
}

void Level::handleMouseMove(const sf::Vector2f mouse)
{
    for (unsigned int i = 0; i < _characters.size(); ++i)
    {
        if (_characters[i]->mouseOver(mouse))
            _characters[i]->setInFocus(true);
        else
            _characters[i]->setInFocus(false);
    }
}

void Level::handleMouseClick(const sf::Vector2f mouse)
{
    for (unsigned int i = 0; i < _characters.size(); ++i)
    {
        if (_characters[i]->mouseOver(mouse))
        {
            chooseNewCharacter(ObjectManager::Characters_E(i));
            _currCharacter = ObjectManager::Characters_E(i);
            break;
        }
    }
}

void Level::chooseNewCharacter(const ObjectManager::Characters_E nowChosen)
{
    _characters[nowChosen]->setChosen(true);
    cleanprevChosen(nowChosen);
}

void Level::cleanprevChosen(const unsigned int nowChosen)
{
    for (unsigned int i = 0; i < _characters.size(); ++i)
    {
        if (i != nowChosen)
            _characters[i]->setChosen(false);
    }
}


bool Level::arrowKey(sf::Keyboard::Key key) const
{
    return key == sf::Keyboard::Up
        || key == sf::Keyboard::Down
        || key == sf::Keyboard::Left
        || key == sf::Keyboard::Right;
}


void Level::show(sf::RenderWindow& window)
{

    updateView();

    window.draw(getShape());

    window.setView(_view);

    _board.show(window);
    showMovables(window);

    window.setView(window.getDefaultView());

    showStatusBar(window);

}

void Level::updateView()
{
    // if the window size is bigger then the board size then
    // the default view should do...
    sf::Vector2f levelSize = _shape.getSize();

    if (levelSize.x >= _boardSize.x
        && levelSize.y >= _boardSize.y)
    {
        return;
    }
    
    sf::Vector2f charPos = _characters[_currCharacter]->getShapePosition();
    sf::Vector2f newView(_view.getCenter());

    if (abs((charPos.x - _view.getCenter().x)) + 50 > _view.getSize().x / 2.f)
    {
        newView.x = charPos.x;
    }

    if (abs((charPos.y - _view.getCenter().y)) + 50 > _view.getSize().y / 2.f)
    {
        newView.y = charPos.y;
    }

    _view.setCenter(newView);

}

void Level::showStatusBar(sf::RenderWindow& window)
{

    if (_status.limitTime)
        _status.time = _level.time - _status.clock.getElapsedTime().asSeconds();
    else
        _status.time = _status.clock.getElapsedTime().asSeconds();

    unsigned int steps;
    if (_status.limitMoves)
    {
        steps = _level.moves - _status.steps;
    }
    else
    {
        steps = _status.steps;
    }

    _statusBar.show(window, _characters[_currCharacter], _status.time, steps);

}

void Level::showMovables(sf::RenderWindow& window) const
{
    for (unsigned int i = 0; i < _characters.size(); ++i)
    {      
        window.draw(_characters[i]->getShape());
    }
}

