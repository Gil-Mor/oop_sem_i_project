#pragma once
#include "Button.h"
class StateCommands;

class ExitButton :
    public Button
{
public:

    ExitButton(const sf::Vector2f& size,
        const sf::Vector2f pos, StateCommands& commands);

    virtual ~ExitButton();

    virtual void action() const;

private:
    StateCommands& _commands;
};

