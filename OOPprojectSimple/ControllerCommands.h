#pragma once

/* Encapsulates calls to member functions of the controller
without exposing the entire controller interface.
different classes with different class can inherit.*/
class Controller;

class ControllerCommands
{
public:
    ControllerCommands(Controller& controller);
    virtual ~ControllerCommands()=0;

protected:

    Controller& _controller;

};

