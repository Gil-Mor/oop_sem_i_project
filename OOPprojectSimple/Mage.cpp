#include "Mage.h"
#include "King.h"
#include "BlankTile.h"
#include "Fire.h"
#include "Gate.h"
#include "Key.h"
#include "Teleport.h"

Mage::Mage()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::MAGE_T));
}


Mage::~Mage()
{
}

void Mage::colide(GameObject& other)
{
    other.colide(*this);
}

void Mage::colide(Mage& other)
{
    return;
}

void Mage::colide(King& other)
{
    return;
}

void Mage::colide(Thief& other)
{
    return;
}

void Mage::colide(Warrior& other)
{
    return;
}

void Mage::colide(BlankTile& other)
{
    moveToPosition(other.getBoardPosition());

}

void Mage::colide(Wall& other)
{
    return;
}

void Mage::colide(Fire& fire)
{
    putOutFire(fire);
    moveToPosition(fire.getBoardPosition());
}

void Mage::putOutFire(Fire& fire)
{
    _commands->setOnBoard(new BlankTile, fire.getBoardPosition());
}

void Mage::colide(Throne& other)
{
    return;
}

void Mage::colide(Gate& other)
{
    if (other.isOpen())
        moveToPosition(other.getBoardPosition());
}

void Mage::colide(Key& other)
{
    moveToPosition(other.getBoardPosition());
}

void Mage::colide(Orc& other)
{
    return;
}

void Mage::colide(Teleport& other)
{
    moveToPosition(other.getBoardPosition());
}

