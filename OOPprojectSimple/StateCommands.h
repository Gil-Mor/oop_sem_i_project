#pragma once
/* Inherits from ControllerCommands.
Encapsulates calls to get and set state member functions
of the controller to get and set the global state of
the program. */

#include "ControllerCommands.h"
#include "Controller.h"

class StateCommands :
    public ControllerCommands
{
public:

    StateCommands(Controller& controller);
    ~StateCommands();

    /* class to set and get state controller member functions. */
    Controller::State_E getState() const;
    void setState(Controller::State_E state);

};

