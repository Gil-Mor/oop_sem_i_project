#pragma once

/* Singleton that Manages the creation of objects for each level.
Collects Characters and keeps them out of the board.
Sets twins for the teleports.*/

#include <vector>
#include "TextureManager.h"
#include "LevelBuilder.h"
#include "Character.h"
#include <unordered_map>

using std::vector;

using std::unordered_map;

class GameObject;
class Character;
class LevelBuilder;
class Teleport;

class ObjectManager
{
public:

    static ObjectManager* getInstance();

    // enum of all objects.
    enum Objects_E
    {
        KING_T = 0,
        MAGE_T,
        THIEF_T,
        WARRIOR_T,

        BLANK_T,
        WALL_T,
        FIRE_T,
        THRONE_T,
        GATE_T,
        KEY_T,
        ORC_T,
        TELEPORT_T,


        NUM_OF_OBJECTS_T
    };


    // special enum for characters.
    enum Characters_E
    {
        KING_C = 0,
        MAGE_C,
        THIEF_C,
        WARRIOR_C,
        NUM_OF_CHARACTERS
    };


    ~ObjectManager();

    void cleanForNewLevel();

    // create an object with info from the struct ObjectInfo from LevelBuilder
    // and a boardPosition (Vector2u).
    GameObject* create(const LevelBuilder::ObjectInfo&, const sf::Vector2u&);

    // Gives the characters that were collected during level creation.
    vector<Character*> getCharacters() const;

    /* return true if the type is a character type. */
    bool isCharacter(const ObjectManager::Objects_E type) const;

    /* return trueif it's a character name.*/
    bool isCharacter(const string& name ) const;


private:

    static ObjectManager* _instance;

    // cotr, copy cotr and assignment operator are private;
    ObjectManager();
    ObjectManager(const ObjectManager&);
    ObjectManager& operator=(const ObjectManager&);


    // vector to collect characters during level loading.
    vector<Character*> _characters;
    void initCharacters();

    // create character.
    void handleCharacter(ObjectManager::Objects_E type, GameObject* object);


    // struct to colect teleports in pairs.
    struct Teleports
    {
        Teleport* a, *b;
    };
    vector<Teleports> _teleports;
    void initTeleports();
    void setTeleportTwin(Teleport* t);

    // return new Object.
    GameObject* getObject(ObjectManager::Objects_E type) const;

    // map from string to enum. Used to conveert the string LevelBuilder
    // is giving..
    unordered_map<string, ObjectManager::Objects_E> _stringToType;

};

