#include "Character.h"
#include "Board.h"
#include "BlankTile.h"
#include "CharacterBoardCommand.h"


const sf::Color FOCUS_COLOR = { 230, 0, 100, 200 };
const sf::Color CHOSEN_COLOR = { 10, 180, 100, 200 };

Character::Character()
    :_commands(nullptr)
{
}


Character::~Character()
{
}

void Character::setCommands(CharacterBoardCommand* commands)
{
    _commands = commands;
}


bool Character::move(const sf::Keyboard::Key key)
{

    sf::Vector2u destination = getNextPositionFromKey(key);

    if (!_commands->validPosition(destination))
    {
        return false;
    }
 
    return move(destination); 
}

bool Character::move(const sf::Vector2u destination)
{
    GameObject* other = _commands->getFromBoard(destination);
    colide(*other);

    return getBoardPosition() == destination;
}




void Character::setInFocus(bool f)
{
    _inFocus = f;
    if (f)
    {
        setColor(FOCUS_COLOR);
    }
    else 
    {
        setColor(DEFAULT_COLOR);
    }

}

void Character::setChosen(bool c)
{
    _chosen = c;
    if (c)
    {
        _shape.setOutlineThickness(2);
        _shape.setOutlineColor(CHOSEN_COLOR);
    }
    else
    {
        _shape.setOutlineColor(sf::Color::Transparent);
    }
}

void Character::moveToPosition(const sf::Vector2u& position)
{
    setBoardPosition(position);
    setShapePosition(_commands->getTileSFMLPosition(position));   
}

sf::Vector2u Character::getNextPositionFromKey(
    const sf::Keyboard::Key key) const
{
    sf::Vector2u pos = getBoardPosition();

    switch (key)
    {
        case (sf::Keyboard::Up):
        {
            pos.y -= 1;
            break;
        }
        case (sf::Keyboard::Down) :
        {
            pos.y += 1;
            break;
        }
        case (sf::Keyboard::Left) :
        {
            pos.x -= 1;
            break;
        }
        case (sf::Keyboard::Right) :
        {
            pos.x += 1;
            break;
        }
    }
    return pos;

}






