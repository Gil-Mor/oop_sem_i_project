/* main function.
Initializes the controller and runs it.*/

#include <iostream>
#include "Controller.h"
using namespace std;



int main()
{

    // construct the Controller.
    Controller c;

    // run the program.
    c.run();

    return 0;
}