bool Character::longMove(const sf::Vector2u destination)
{
    vector<sf::Vector2u> path;

    bool success = BFS(path, destination);
}

bool Character::BFS(vector<sf::Vector2u>& path, const sf::Vector2u destination)
{

    std::queue <BFSnode> q;

    BFSnode source;
    source.color = 0;
    source.distance = 0;
    source.pos = getBoardPosition();
    source.parent = source.pos;

    q.push(source);

    bool found = false;
    while (!q.empty())
    {
        BFSnode current = q.front();
        q.pop();

        current.color = 1;
        if (visitNeighbors(current, q, destination))
        {
            found = true;
            break;
        }

    }
    
}

bool Character::visitNeighbors(struct BFSnode current, 
    std::queue<struct BFSnode>& q, const sf::Vector2u dest)
{
    bool found = false;
    for (unsigned int i = current.pos.y - 1; i <= current.pos.y + 1; ++i)
    {
        for (unsigned int j = current.pos.x - 1; j <= current.pos.x + 1; ++j)
        {
            if (!_commands->validPosition({ i, j }))
                continue;

            BFSnode neighbor = 
        }
    }
}