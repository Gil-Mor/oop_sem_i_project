#pragma once

/* Matrix of GameObject* as the gameBoard. */

#include "Drawable.h"
#include <vector>
#include <fstream>
#include "Wall.h"
class GameObject;
class Movable;
class Character;

using std::ifstream;
using std::vector;

class Board : public mydrawable::Drawable
{
public:

    Board();

    virtual ~Board();

    /* create the board shape.. background etc..*/
    void create(const sf::Vector2f& shapeSize, 
        const sf::Vector2f& pos);

    /* set a board. */
    void setBoard(vector<vector<GameObject* > >&, vector<Character*>& movables);

    void show(sf::RenderWindow& window) const;

    /* Sets an object at the requested position. */
    void setOnBoard(GameObject* object, const sf::Vector2u& pos);

    /* get the object in the requested position. */
    GameObject* getFromBoard(const sf::Vector2u& pos) const;

    /* get the SFML position of a tile.*/
    sf::Vector2f getTileSFMLPosition(const sf::Vector2u pos) const;

    /* return true if it's a valid position. */
    bool validPosition(const sf::Vector2u pos) const;

    /* returntrue if the thief has a key. 
    this is used by the warrior to know if to 
    drop more keys.*/
    bool thiefHasKey() const;


private:

    /* 2d vector of game objects is the game board.*/
    vector<vector<GameObject*> > _board;

    /* the characters. they're not on the board. */
    vector<Character*> _characters;

    /* number o ftiles in the board.*/
    sf::Vector2u _size;

    /* Walls that are drawn around the board.*/
    vector<Wall> _surroundingWalls;
    void setSurroundingWalls();

    /* Give the characters positoin according to their board position.*/
    void Board::giveMovablePositions(vector<Character*>& movables);

    /* return the character in that baordPositoin. */
    Character* movableInPosition(const sf::Vector2u& pos) const;

    /* draw the surrounding walls. */
    void showSurroundingWalls(sf::RenderWindow& window) const;



};



