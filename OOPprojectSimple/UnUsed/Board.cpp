#include "Board.h"
#include "TextureManager.h"

#include "StateCommands.h"
#include "ObjectManager.h"
#include "Character.h"
#include "BlankTile.h"
#include "Wall.h"
#include <string>

using std::string;


Board::Board(StateCommands& commands)
    :_stateCommands(commands),
    _characterCommnads(*(new CharacterBoardCommand(*this))),
    _currCharacter(0)
{
    //_stateMan = StateManager::getInstance();
    _characters = ObjectManager::getInstance()->getCharacters();
}

void Board::create(const sf::RenderWindow& window)
{
    // create board shape..
    _windowSize = sf::Vector2f(window.getSize());

    _backGround.setPosition(sf::Vector2f(window.getPosition()));
    _backGround.setSize(sf::Vector2f(window.getSize()));

    _backGround.setTexture(TextureManager::getInstance()->
        getBoardTex()[Board_Tex::BACKGROUND_T]);

}


Board::~Board()
{
    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            delete _board[i][j];

        }
        _board[i].clear();
    }
    _board.clear();
}

void Board::run(sf::RenderWindow& window)
{

    while (_stateCommands.getState() == Controller::State_E::Playing_S)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {

            switch (event.type)
            {
                case sf::Event::KeyPressed:
                {
                    handleKeyBoard(event.key.code);
                    break;
                }

                case sf::Event::Closed:
                    _stateCommands.setState(Controller::State_E::Exiting_S);
                    break;

                default:
                    break;
            }
        }

        window.clear();
        show(window);
        window.display();
    }


}


void Board::handleKeyBoard(sf::Keyboard::Key key)
{
    if (key == sf::Keyboard::P)
    {
        _currCharacter = (_currCharacter + 1) % _characters.size();
    }
    else if (arrowKey(key))
    {
        _characters[_currCharacter]->move(key, &_characterCommnads);
    }

    else if (key == sf::Keyboard::Escape)
    {
        _stateCommands.setState(Controller::State_E::Exiting_S);
    }
}

bool Board::arrowKey(sf::Keyboard::Key key) const
{
    return key == sf::Keyboard::Up
        || key == sf::Keyboard::Down
        || key == sf::Keyboard::Left
        || key == sf::Keyboard::Right;
}


void Board::show(sf::RenderWindow& window) const
{
    window.draw(_backGround);

    showSurroundingWalls();

    BlankTile BG;
    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            BG.setShapePosition(_board[i][j]->getShapePosition());
            window.draw(BG.getShape());
            _board[i][j]->show(window);

        }
    }


}


void Board::setLevel(vector<vector<GameObject* > >& board)
{
    _board = board;

    // give them SFML positions
    sf::Vector2f startPos;

    startPos.x = (_windowSize.x - (_board[0].size()*OBJECT_SIZE.x)) / 2.f;

    startPos.y = (_windowSize.y - (_board.size()*OBJECT_SIZE.y)) / 2.f;

    _boardSFMLSize = { startPos.x*(_board[0].size()*OBJECT_SIZE.x), startPos.y*(_board.size()*OBJECT_SIZE.y) };


    sf::Vector2f pos = startPos;

    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            _board[i][j]->setShapePosition(pos);
            pos.x += OBJECT_SIZE.x;
        }
        pos.x = startPos.x;
        pos.y += OBJECT_SIZE.y;
    }

    setSurroundingWalls();
}

void Board::setSurroundingWalls()
{
    _surroundingWalls.resize(_board.size() + 2);

    sf::RectangleShape wall(OBJECT_SIZE);
    wall.setFillColor({ 220, 100, 220, 255 });

    sf::Vector2f wallPos(_board[0][0]->getShapePosition());


    for (unsigned int i = 0; i < _board.size(); ++i)
    {

        wallPos = _board[i][0]->getShapePosition();

        wallPos.x -= OBJECT_SIZE.x;
        wall.setPosition(wallPos);

        wallPos = _board[i][_board.size() - 1]->getShapePosition();
        wallPos.x += OBJECT_SIZE.x;
        wall.setPosition(wallPos);


    }

    for (unsigned int i = 0; i < _board[0].size(); ++i)
    {

        wallPos = _board[0][i]->getShapePosition();

        wallPos.y -= OBJECT_SIZE.y;
        wall.setPosition(wallPos);

        wallPos = _board[_board[i].size() - 1][i]->getShapePosition();
        wallPos.y += OBJECT_SIZE.y;
        wall.setPosition(wallPos);


    }


}

void Board::setOnBoard(GameObject* object, const sf::Vector2u& pos)
{
    object->setBoardPosition(pos);

    object->setShapePosition(_board[pos.y][pos.x]->getShapePosition());

    _board[pos.y][pos.x] = object;

}

GameObject* Board::getFromBoard(const sf::Vector2u& pos) const
{
    return _board[pos.y][pos.x];
}

bool Board::validPosition(const sf::Vector2u pos) const
{
    return 0 <= pos.y && 0 <= pos.x
        && pos.y < _board.size()
        && pos.x < _board[0].size();
}

//void Board::drawCharacters(sf::RenderWindow& window) const
//{
//    for (unsigned int i = 0; i < _characters.size(); ++i)
//    {
//        _characters[i]->show(window);
//    }
//}


//void Board::giveCharactersPositions()
//{
//    for (unsigned int i = 0; i < _characters.size(); ++i)
//    {
//        sf::Vector2u pos = _characters[i]->getBoardPosition();
//        _characters[i]->setShapePosition(_board[pos.y][pos.x]->getShapePosition());
//    }
//}