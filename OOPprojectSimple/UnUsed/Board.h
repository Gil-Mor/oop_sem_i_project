#pragma once
#include "CharacterBoardCommand.h"
#include "GameObject.h"
#include "Character.h"
#include <vector>
#include <fstream>

//class StateManager;
class StateCommands;

using std::ifstream;
using std::vector;

class Board
{
public:

    Board(StateCommands& _stateCommands);

    ~Board();

    void create(const sf::RenderWindow& window);

 
    void setLevel(vector<vector<GameObject* > >&);

    void setBoardoard(ifstream& levelFile);

    void show(sf::RenderWindow& window) const;

    void run(sf::RenderWindow& window);

    void setOnBoard(GameObject* object, const sf::Vector2u& pos);

    GameObject* getFromBoard(const sf::Vector2u& pos) const;

    bool validPosition(const sf::Vector2u pos) const;

private:

    /* 2d vector of game objects is the game board.*/
    vector<vector<GameObject*> > _board;

    vector<Character*> _characters;
    unsigned int _currCharacter;

    //void giveCharactersPositions();
    //void drawCharacters(sf::RenderWindow& window) const;

    //StateManager* _stateMan;
    StateCommands& _stateCommands;

    sf::RectangleShape _backGround;
    
    vector<sf::RectangleShape> _surroundingWalls;
    void setSurroundingWalls();

    sf::Vector2f _windowSize;
    sf::Vector2f _boardSFMLSize;

    void handleKeyBoard(sf::Keyboard::Key key);
    bool arrowKey(sf::Keyboard::Key key) const;

    CharacterBoardCommand& _characterCommnads;

};



