#include "StateManager.h"

StateManager* StateManager::_instance = nullptr;

StateManager::StateManager()
    :_state(StateManager::State_E::ShowingMenu_S)
{
}


StateManager::~StateManager()
{
}

StateManager* StateManager::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new StateManager;
    }
    return _instance;
}

StateManager::State_E StateManager::getState() const
{
    return _state;
}

void StateManager::setState(StateManager::State_E state)
{
    _state = state;
}
