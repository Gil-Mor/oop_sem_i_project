#pragma once
class StateManager
{

public:

    /* The currnet state of the program.
    Used to channel events.
    */
    enum State_E
    {
        Uninitialized_S = 0,
        Playing_S,
        Pause_s,
        ShowingMenu_S,
        Exiting_S
    };

    static StateManager* getInstance();

    StateManager::State_E getState() const;
    void setState(StateManager::State_E state);

    ~StateManager();

private:


    static StateManager* _instance;


    State_E _state;


    // private cotr, copy cotr, and = operator.
    StateManager();
    StateManager(const StateManager&) = delete;
    StateManager& operator=(const StateManager&) = delete;

};

