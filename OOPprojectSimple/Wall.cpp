#include "Wall.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Wall::Wall()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::WALL_T));
}


Wall::~Wall()
{
}

void Wall::colide(GameObject& other)
{
    other.colide(*this);
}

void Wall::colide(Mage& other)
{
    other.colide(*this);
}

void Wall::colide(King& other)
{
    other.colide(*this);
}

void Wall::colide(Thief& thief)
{
    thief.colide(*this);
}

void Wall::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
