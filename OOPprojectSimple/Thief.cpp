#include "Thief.h"
#include "TextureManager.h"
#include "CharacterBoardCommand.h"
#include "BlankTile.h"
#include "Fire.h"
#include "Gate.h"
#include "Key.h"
#include "Teleport.h"

Thief::Thief()
    :_hasKey(false)
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::Objects_E::NO_KEY_THIEF_T));

}

Thief::~Thief()
{
}

bool Thief::hasKey() const
{
    return _hasKey;
}

void Thief::grabKey(Key& key)
{

    _hasKey = true;
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::Objects_E::KEY_THIEF_T));
    _commands->setOnBoard(new BlankTile, key.getBoardPosition());
}

void Thief::loseKey()
{
    _hasKey = false;
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::Objects_E::NO_KEY_THIEF_T));

}

void Thief::openGate(Gate& gate)
{
    gate.setOpen(true);
    loseKey();
}


void Thief::colide(GameObject& other)
{
    other.colide(*this);
}


void Thief::colide(Thief& other)
{
    return;
}

void Thief::colide(King& other)
{
    return;
}

void Thief::colide(Mage& other)
{
    return;
}
void Thief::colide(Warrior& other)
{
    return;
}

void Thief::colide(BlankTile& other)
{
    moveToPosition(other.getBoardPosition());

}

void Thief::colide(Wall& other)
{
    return;
}

void Thief::colide(Fire& fire)
{
    return;
}

void Thief::colide(Throne& other)
{
    return;
}

void Thief::colide(Gate& gate)
{

    if (gate.isOpen())
    {
        moveToPosition(gate.getBoardPosition());
        return;
    }

    if (hasKey()) {
        openGate(gate);
        moveToPosition(gate.getBoardPosition());
        return;
    }

}

void Thief::colide(Key& key)
{
    if (!hasKey())
    {
        grabKey(key);
    }

    moveToPosition(key.getBoardPosition());
}

void Thief::colide(Orc& other)
{
    return;
}

void Thief::colide(Teleport& other)
{
    GameObject* atTwin = _commands->getFromBoard(other.getTwinPosition());
    if (typeid(*atTwin) != typeid(other))
        return;


    moveToPosition(other.getTwinPosition());
}



