#include "Key.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Key::Key()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::KEY_T));
}


Key::~Key()
{
}

void Key::colide(GameObject& other)
{
    other.colide(*this);
}
void Key::colide(Mage& other)
{
    other.colide(*this);
}

void Key::colide(King& other)
{
    other.colide(*this);
}

void Key::colide(Thief& thief)
{
    thief.colide(*this);
}

void Key::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
