#include "StateCommands.h"
#include "Controller.h"

StateCommands::StateCommands(Controller& controller)
    :ControllerCommands(controller)
{
}


StateCommands::~StateCommands()
{
}

Controller::State_E StateCommands::getState() const
{
    return _controller.getState();
}

void StateCommands::setState(Controller::State_E state)
{
    _controller.setState(state);
}
