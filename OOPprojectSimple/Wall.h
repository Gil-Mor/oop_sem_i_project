#pragma once
#include "GameObject.h"
class Wall :
    public GameObject
{
public:
    Wall();
    ~Wall();

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);
};

