#include "Button.h"
#include "Controller.h"

const sf::Color BUTTON_FOCUS_COLOR = { 150, 0, 0, 200 };
const sf::Color BUTTON_NO_FOCUS_COLOR = { 204, 204, 0, 200 };

const sf::Color BUTTON_PRESSED_COLOR = { 200, 32, 180, 250 };

const sf::Color BUTTON_BG_NO_FOCUS_COLOR = { 255, 100, 255, 255 };
const sf::Color BUTTON_BG_FOCUS_COLOR = { 50, 232, 240, 250 };

Button::Button()
{
}

Button::Button(const sf::Vector2f& size, const sf::Vector2f& pos)
{
    _shape.setSize(size);
    _shape.setPosition(pos);
    _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
}


Button::~Button()
{
}

void Button::setInFocus(bool f)
{
    _inFocus = f;
    if (f)
    {
        _shape.setFillColor(BUTTON_FOCUS_COLOR);
    }
    else
    {
        _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
    }
}

void Button::setChosen(bool c)
{
    _chosen = c;
    if (c)
        _shape.setFillColor(BUTTON_PRESSED_COLOR);
    else
        _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
}

