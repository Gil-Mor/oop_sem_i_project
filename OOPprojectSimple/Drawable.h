#pragma once

/* Keeps a sf::RectangleShape for each class that needs it.
Every class that even needs to draw a background inherits from
this class. */
#include <SFML\Graphics.hpp>

// default color white doesn't render with textures
// leaving them unchanged.
const sf::Color DEFAULT_COLOR = sf::Color::White;

namespace mydrawable
{
    class Drawable
    {
    public:

        Drawable();
        Drawable(const sf::RectangleShape& shape);

        // make class abstract.
        virtual ~Drawable()=0;

        // draw the shape to the window.
        void show(sf::RenderWindow& window) const;

        const sf::RectangleShape& getShape() const;

    protected:
        /* Rectangle shape has all the neccesery charicaristics
        That we need to draw everything to an sfml window.
        Size, Position, Texture, Color etc...
        By making the shape protected i make it a private member
        of each derivedclass so classes that need to change their 
        own shape like buttons and characters can do this easily.
        */
        sf::RectangleShape _shape;
    };

}