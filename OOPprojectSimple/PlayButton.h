#pragma once
#include "Button.h"
class StateCommands;

class PlayButton :
    public Button
{
public:
    PlayButton(const sf::Vector2f& size,
        const sf::Vector2f& pos, StateCommands& commands);

    virtual ~PlayButton();

    virtual void action() const;

private:
    StateCommands& _commands;
};

