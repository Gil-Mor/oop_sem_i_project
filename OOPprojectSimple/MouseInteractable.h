#pragma once

/* Class representing objects that interact wit the mouse.
These objects can check if the mouse is hovering above
them and get to inFoscus mode.
Also they can can be setto chosen if the mouse clickedthem.
Buttons and GameObjects are derived from this class. */
#include "Drawable.h"
class MouseInteractable :
    public mydrawable::Drawable

{

public:
    MouseInteractable();

    virtual ~MouseInteractable()=0;

    /* Returns true if the mouse is over this object RectangleShape. 
    the mouse coordinatesare given in 'world' coordinates - 
    from window.mapPixelToCoords function. 
    That's why Vector2f and not 2i. */
    bool mouseOver(const sf::Vector2f& mouse) const;

    /* returns true if this object is currently in focus.*/
    bool getInFocus() const;

    /* returns true if this object is currently chosen.*/
    bool getChosen() const;

    /* Set the object i nand out of focus. */
    virtual void setInFocus(bool f);

    /* Set the object chosen flag.*/
    virtual void setChosen(bool c);

protected:
    
    bool _inFocus;
    bool _chosen;
};

