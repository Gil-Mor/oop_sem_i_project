#pragma once
#include "GameObject.h"
class Throne :
    public GameObject
{
public:
    Throne();
    ~Throne();

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);
};

