#include "TextureManager.h"
#include "GameObject.h"
#include <string>
#include <iostream>
#include <fstream>
using std::ifstream;
using std::string;

const string TEXTURE_DIR = "../SourceTextures/";
const string BOARD_FILE = "boardTextures.txt";
const string OBJECTS_FILE = "objectsTextures.txt";
const string MENU_FILE = "menuTextures.txt";
const string CONTROLLER_FILE = "controllerTextures.txt";


TextureManager* TextureManager::_instance = nullptr;

TextureManager::TextureManager()
{
    loadTextures(_boardTextures, BOARD_FILE);
    loadTextures(_menuTextures, MENU_FILE);
    loadTextures(_objectsTextures, OBJECTS_FILE);
    loadTextures(_controllerTextures, CONTROLLER_FILE);
}


void TextureManager::loadTextures(vector<const sf::Texture*>& v,
    const string fileName)
{
    ifstream file;
    file.open(TEXTURE_DIR + fileName);
    if (!file.is_open())
    {
        return;
    }

    string line;

    while (getline(file, line))
    {
        // only support png and jpg..
        if (line.find(".png") != string::npos
            || line.find(".jpg") != string::npos)
        {

            sf::Texture* t = new sf::Texture;
            t->loadFromFile(TEXTURE_DIR + line);

            v.push_back(t);
        }
    }

    file.close();
  
}

TextureManager::~TextureManager()
{
    for (unsigned int i = 0; i < _boardTextures.size(); ++i)
    {
        delete _boardTextures[i];
    }
    for (unsigned int i = 0; i < _menuTextures.size(); ++i)
    {
        delete _menuTextures[i];
    }

    delete _instance;
}

TextureManager* TextureManager::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new TextureManager;
    }
    return _instance;
}


const sf::Texture* TextureManager::getMenuTex(Menu_Tex::Menu_E tex) const
{
    return _menuTextures[tex];
}

const sf::Texture* TextureManager::getControllerTex(
    Controller_Tex::Controller_E tex) const
{
    return _controllerTextures[tex];
}

const sf::Texture* TextureManager::getBoardTex(Board_Tex::Board_E tex) const
{
    return _boardTextures[tex];
}


const sf::Texture* TextureManager::getObjectsTex(Objects_Tex::Objects_E object) const
{
    return _objectsTextures[object];
}



