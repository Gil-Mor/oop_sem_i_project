#include "Orc.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Orc::Orc()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::ORC_T));
}


Orc::~Orc()
{
}

void Orc::colide(GameObject& other)
{
    other.colide(*this);
}
void Orc::colide(Mage& other)
{
    other.colide(*this);
}

void Orc::colide(King& other)
{
    other.colide(*this);
}

void Orc::colide(Thief& thief)
{
    thief.colide(*this);
}

void Orc::colide(Warrior& warrior)
{
    warrior.colide(*this);
}

