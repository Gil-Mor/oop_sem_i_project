#pragma once

/* Controls the flow of the program.
Channels control to different parts of the program
like the Menu, Level(game), messages to the user etc..
*/

#include "Level.h"
#include "Menu.h"
#include "GameObject.h"
#include "LevelBuilder.h"

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <vector>
#include <string>

using std::vector;
using std::string;

class StateCommands;

class Controller
{
public:


    /* The currnet state of the program.
    Used to channel control.
    */
    enum State_E
    {
        Uninitialized_S = 0,
        ShowingMenu_S,
        Playing_S,
        Winning_S, 
        Losing_S,
        FinishedLevels_S,
        Exiting_S
    };

    Controller();
    virtual ~Controller();

    /* Runs a loop that channels control to differnt
    parts according to the current state. */
    void run();

    /* Get the current state of the program. */
    Controller::State_E getState() const;

    /* Set the currnet state of the program. 
    This is used by defferent parts of the program
    through a StateCommand class which encapsulates
    a call to this function. */
    void setState(Controller::State_E state);

    /* Set the volume of the soundtrack.
    used by the volume buttons.*/
    void setVolume(const float volume);
    float getVolume() const;

private:

    /* SFML window. */
    sf::RenderWindow _mainWindow;

    /* current stateo of the program. */
    State_E _state;

    /* class which encapsulates calls to getState
    and setState member functions. 
    this is given to the menu and to Level*/
    StateCommands& _stateCommands;

    /* level loader. ask for levels from this.*/
    LevelBuilder _levelBuilder;

    /* struct defined in LevleBuilder.
    holds information about the levelf like
    th eboard, time limit, move limit..*/
    LevelBuilder::LevelStruct _levelStruct;

    /* class controlling the currnet level being played.*/
    Level _level;

    /* the main menu. */
    Menu _menu;

    /* the currnet levelf being played. starts from 0.*/
    unsigned int _currLevel;


    void loadLevel();
    void cleanAfterLevel();

    /* stuff for displaying messages. */
    struct Message
    {
        sf::RectangleShape _messageBG;
        sf::Text text;
        sf::Font font;
    } _message;


    void displayMessage(const string message, unsigned int fontSize);
    void InitMessageDisplay();
  
    void InitWindow();
    
    /* functions for wininng state, losing state, finish all levels state.*/
    void win();
    void lose();
    void finish();

    /* sound stuff*/
    sf::Music _soundTrack;
    sf::SoundBuffer _winSoundBuf;
    sf::Sound _winSound;
    void InitSounds();


};



