#pragma once
#include "Character.h"
#include <SFML\Audio.hpp>

class King :
    public Character
{
public:

    King();
    virtual ~King();

    bool onThrone() const;

    //virtual void move(
    //    const sf::Keyboard::Key key,
    //    vector<vector<GameObject*> >& board);


    virtual void colide(GameObject& other);
    virtual void colide(King& other);
    virtual void colide(Mage& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);
    virtual void colide(BlankTile& other);
    virtual void colide(Wall& other);
    virtual void colide(Fire& other);
    virtual void colide(Throne& other);
    virtual void colide(Gate& other);
    virtual void colide(Key& other);
    virtual void colide(Orc& other);
    virtual void colide(Teleport& other);

private:

    bool _onThrone;
    sf::SoundBuffer _soundBuffer;

};

