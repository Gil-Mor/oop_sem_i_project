#include "MouseInteractable.h"

MouseInteractable::MouseInteractable()
    : _inFocus(false), _chosen(false)
{}

MouseInteractable::~MouseInteractable()
{}

bool MouseInteractable::mouseOver(const sf::Vector2f& mouse) const
{
    float startX = getShape().getPosition().x;

    float startY = getShape().getPosition().y;

    float endX = (startX + getShape().getSize().x);

    float endY = (startY + getShape().getSize().y);

    return startX < mouse.x && mouse.x < endX
        && startY < mouse.y && mouse.y < endY;
}

void MouseInteractable::setInFocus(bool f)
{
    _inFocus = f;
}

void MouseInteractable::setChosen(bool c)
{
    _chosen = c;
}

bool MouseInteractable::getInFocus() const
{
    return _inFocus;
}

bool MouseInteractable::getChosen() const
{
    return _chosen;
}
