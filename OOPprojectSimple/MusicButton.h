#pragma once
#include "Button.h"
#include <SFML\Audio.hpp>

class MusicButton : public Button
{
public:

    MusicButton(const sf::Vector2f& size, 
        const sf::Vector2f& pos, sf::Music& soundTrack);

    ~MusicButton();

    virtual void action()const=0;

protected:
    sf::Music& _soundTrack;
};

