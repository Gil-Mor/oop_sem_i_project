#include "VolumeButton.h"
#include "TextureManager.h"

VolumeButton::VolumeButton(const sf::Vector2f& size, const sf::Vector2f& pos,
    sf::Music& soundTrack, bool whichWay)
    : MusicButton(size, pos, soundTrack), _up(whichWay)
{
    if (whichWay)
    {
        _shape.setTexture(TextureManager::getInstance()->
            getMenuTex(Menu_Tex::VOLUME_UP_T));
    }
    else
    {
        _shape.setTexture(TextureManager::getInstance()->
            getMenuTex(Menu_Tex::VOLUME_DOWN_T));
    }

    _shape.setFillColor(sf::Color::White);
  
}


VolumeButton::~VolumeButton()
{
}

void VolumeButton::action() const
{
    float currVol = _soundTrack.getVolume();

    if (_up)
    {
        _soundTrack.setVolume(currVol *(6.f/5.f));
    }
    else
    {
        _soundTrack.setVolume(currVol * (5.f/6.f));
    }
}
