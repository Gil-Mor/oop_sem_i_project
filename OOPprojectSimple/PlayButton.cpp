#include "PlayButton.h"
#include "StateCommands.h"

PlayButton::PlayButton(const sf::Vector2f& size,
    const sf::Vector2f& pos, StateCommands& commands)
    : Button(size, pos), _commands(commands)
{
    _shape.setTexture(TextureManager::getInstance()->
        getMenuTex(Menu_Tex::PLAY_T));
    _shape.setFillColor(sf::Color::White);
}



PlayButton::~PlayButton()
{
}

void PlayButton::action() const
{
    _commands.setState(Controller::State_E::Playing_S);
}
