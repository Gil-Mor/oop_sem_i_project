#include "Board.h"
#include "TextureManager.h"
#include "ObjectManager.h"
#include "Character.h"
#include "BlankTile.h"
#include "Thief.h"
#include "Wall.h"
#include <string>

using std::string;

Board::Board()
{

}

void Board::create(const sf::Vector2f& shapeSize, const sf::Vector2f& pos)
{

    _shape.setSize(shapeSize);

    _shape.setPosition(pos);

    _shape.setFillColor({ 255, 255, 255, 100 });


}

Board::~Board()
{
}


void Board::show(sf::RenderWindow& window) const
{
    window.draw(getShape());

    showSurroundingWalls(window);

    BlankTile BG;
    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            BG.setShapePosition(_board[i][j]->getShapePosition());
            window.draw(BG.getShape());
            _board[i][j]->show(window);

        }
    }
}

void Board::showSurroundingWalls(sf::RenderWindow& window) const
{
    for (unsigned int i = 0; i < _surroundingWalls.size(); ++i)
    {
        window.draw(_surroundingWalls[i].getShape());
    }

}


void Board::setBoard(vector<vector<GameObject* > >& board, vector<Character*>& characters)
{
    // create board shape..
    _surroundingWalls.clear();
    _board = board;
    _size = { board[0].size(), board.size() };

    _characters = characters;

    sf::Vector2f startPos = _shape.getPosition();
    sf::Vector2f pos = startPos;

    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            _board[i][j]->setShapePosition(pos);
            pos.x += OBJECT_SIZE.x;
        }
        pos.x = startPos.x;
        pos.y += OBJECT_SIZE.y;
    }
    giveMovablePositions(characters);
    setSurroundingWalls();
}

void Board::giveMovablePositions(vector<Character*>& movables)
{
    sf::Vector2u pos;
    for (unsigned int i = 0; i < movables.size(); ++i)
    {
        pos = movables[i]->getBoardPosition();
        movables[i]->setShapePosition(_board[pos.y][pos.x]->getShapePosition());
    }
}

void Board::setSurroundingWalls()
{

    Wall wall;
    wall.setColor({ 220, 100, 220, 255 });

    sf::Vector2f wallPos(_board[0][0]->getShapePosition());

    wall.setShapePosition(wallPos - OBJECT_SIZE);
    _surroundingWalls.push_back(wall);

    unsigned int i = 0;
    for (; i < _board.size(); ++i)
    {

        wallPos = _board[i][0]->getShapePosition();

        wallPos.x -= OBJECT_SIZE.x;
        wall.setShapePosition(wallPos);
        _surroundingWalls.push_back(wall);

        wallPos = _board[i][_size.x - 1]->getShapePosition();
        wallPos.x += OBJECT_SIZE.x;
        wall.setShapePosition(wallPos);

        _surroundingWalls.push_back(wall);

    }

    for (unsigned int i = 0; i < _board[0].size(); ++i)
    {

        wallPos = _board[0][i]->getShapePosition();

        wallPos.y -= OBJECT_SIZE.y;
        wall.setShapePosition(wallPos);
        _surroundingWalls.push_back(wall);

        wallPos = _board[_size.y - 1][i]->getShapePosition();
        wallPos.y += OBJECT_SIZE.y;
        wall.setShapePosition(wallPos);
        _surroundingWalls.push_back(wall);

    }

}

sf::Vector2f Board::getTileSFMLPosition(const sf::Vector2u pos) const
{
    return _board[pos.y][pos.x]->getShapePosition();
}


void Board::setOnBoard(GameObject* object, const sf::Vector2u& pos)
{
    object->setBoardPosition(pos);

    object->setShapePosition(_board[pos.y][pos.x]->getShapePosition());

    _board[pos.y][pos.x] = object;

}

GameObject* Board::getFromBoard(const sf::Vector2u& pos) const
{
    Movable* m = movableInPosition(pos);

    if (m)
    {
        return m;
    }
    return _board[pos.y][pos.x];
}

Character* Board::movableInPosition(const sf::Vector2u& pos) const
{
    for (unsigned int i = 0; i < _characters.size(); ++i)
    {
        if (_characters[i]->getBoardPosition() == pos)
            return _characters[i];
    }
    return nullptr;
}

bool Board::validPosition(const sf::Vector2u pos) const
{
    return 0 <= pos.y && 0 <= pos.x
        && pos.y < _board.size()
        && pos.x < _board[0].size();
}

bool Board::thiefHasKey() const
{
    Thief* f = dynamic_cast<Thief*>
        (_characters[ObjectManager::Characters_E::THIEF_C]);
    if (f)
        return f->hasKey();
    return false;
}
