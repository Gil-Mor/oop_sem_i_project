#include "LevelStatusBar.h"
#include "TextureManager.h"
#include "GameObject.h"

const string FONT_FILE_NAME = "../SourceFont/impact.ttf";
const sf::Color TEXT_COLOR = sf::Color::White;
const sf::Vector2f CURRENT_CHARACTER_TEXTURE_SIZE = { 100, 100 };


LevelStatusBar::LevelStatusBar()
{
    _shape.setFillColor(sf::Color::Transparent);

    _currCharacterImage.setSize(CURRENT_CHARACTER_TEXTURE_SIZE);

    _font.loadFromFile(FONT_FILE_NAME); 

    _time.setFont(_font);
    _moves.setFont(_font);

    _time.setCharacterSize(15);
    _moves.setCharacterSize(15);

    _time.setColor(TEXT_COLOR);
    _moves.setColor(TEXT_COLOR);

}


LevelStatusBar::~LevelStatusBar()
{
}

void LevelStatusBar::create(
    const sf::Vector2f size, 
    const sf::Vector2f pos,
    bool timeLimit,
    bool movesLimit)
{

    if (timeLimit)
    {
        _timePrefix = "Time Left:  ";
    }
    else
    {
        _timePrefix = "Time:  ";
    }

    if (movesLimit)
    {
        _movesPrefix = "Moves Left:  ";
    }
    else
    {
        _movesPrefix = "Moves:  ";
    }
    _shape.setPosition(pos);

    _shape.setSize(size);

    sf::Vector2f stuffPos = { pos.x + 10, pos.y + 50};

    _currCharacterImage.setPosition(stuffPos);

    stuffPos.y += 50 + CURRENT_CHARACTER_TEXTURE_SIZE.y;
    //stuffPos.x += 10;

    _time.setPosition(stuffPos);

    stuffPos.y += 50;

    _moves.setPosition(stuffPos);


}


// Can't be const because changing members here.. setTexture, setstring..
void LevelStatusBar::show(
    sf::RenderWindow& window, 
    const GameObject* currCharacter, 
    float time, unsigned int steps)
{
    _currCharacterImage.setTexture(currCharacter->getTexture());
    _time.setString(_timePrefix + std::to_string((int)time));
    _moves.setString(_movesPrefix + std::to_string(steps));

    window.draw(_shape);
    window.draw(_currCharacterImage);
    window.draw(_time);
    window.draw(_moves);

}
