#pragma once
#include "GameObject.h"

class Teleport : public GameObject
{
public:

    Teleport();
    ~Teleport();

    void setTwin(const Teleport* twin);
    sf::Vector2u getTwinPosition() const;

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);;

private:

    const Teleport* _twin;
};

