#pragma once
#include "GameObject.h"
class BlankTile :
    public GameObject
{
public:
    BlankTile();
    ~BlankTile();

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);
};

