#pragma once

/* Incharge Of Running a single level in the game.
holds the board and a status bar.*/

#include "Drawable.h"
#include "CharacterBoardCommand.h"
#include "LevelBuilder.h"
#include "Board.h"
#include "GameObject.h"
#include "ObjectManager.h"
#include "Character.h"
#include "LevelStatusBar.h"
#include "Chronometer.h"

#include <vector>
#include <fstream>

class StateCommands;

using std::ifstream;
using std::vector;

class Level :public mydrawable::Drawable
{
public:

    Level(StateCommands& _stateCommands);

    ~Level();

    /* creates the shape of the level window. */
    void create(const sf::RenderWindow& window);

    /* Sets a new Level. */
    void setNewLevel(LevelBuilder::LevelStruct& level);

    /* Shows the level.*/
    void show(sf::RenderWindow& window);

    void run(sf::RenderWindow& window);

private:

    LevelBuilder::LevelStruct _level;


    Board _board;

    // _boardSize is used for the view calculations
    sf::Vector2f _boardSize;

    /* The board size depends on the number of tiles and their size. */
    sf::Vector2f calcBoardSize();

    /* Set the board in the middle of the level window.*/
    sf::Vector2f calcBoardPos(const sf::Vector2f boardSize) const;

    // ======== STATUS BAR ==============
    
    LevelStatusBar _statusBar;
    //sf::Vector2f calcStatusBarPos() const;
    struct Status
    {
        sftools::Chronometer clock;
        float time;
        int steps;
        bool limitMoves, limitTime;

    } _status;
    void initStatus();
    void showStatusBar(sf::RenderWindow& window) ;
    void checkStatus();
    //------------------------------------------

    vector<Character*> _characters;

    /* for checking if the king is on the throne..*/
    King* _king;
    ObjectManager::Characters_E _currCharacter;

    CharacterBoardCommand& _characterCommnads;
    void giveCommands();
    void Level::showMovables(sf::RenderWindow& window) const;

    StateCommands& _stateCommands;


    void handleKeyBoard(sf::Keyboard::Key key);
    bool arrowKey(sf::Keyboard::Key key) const;
    void handleMouseMove(const sf::Vector2f mouse);
    void handleMouseClick(const sf::Vector2f mouse);
    void chooseNewCharacter(const ObjectManager::Characters_E nowChosen);
    void cleanprevChosen(const unsigned int nowChosen);

    sf::View _view;
    void updateView();

};



