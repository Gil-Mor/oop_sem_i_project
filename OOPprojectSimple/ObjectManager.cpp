#include "ObjectManager.h"
#include "GameObject.h"
#include "TextureManager.h"
#include "LevelBuilder.h"
#include "King.h"
#include "Mage.h"
#include "Thief.h"
#include "Warrior.h"
#include "BlankTile.h"
#include "Wall.h"
#include "Fire.h"
#include "Throne.h"
#include "Gate.h"
#include "Key.h"
#include "Orc.h"
#include "Teleport.h"
#include <memory>


const unsigned int COUPLES_OF_TELEPORTS = 4;

ObjectManager* ObjectManager::_instance = nullptr;

ObjectManager::ObjectManager()
{

    _stringToType.insert(std::make_pair("king", ObjectManager::KING_T));
    _stringToType.insert(std::make_pair("mage", ObjectManager::MAGE_T));
    _stringToType.insert(std::make_pair("no_key_thief", ObjectManager::THIEF_T));
    _stringToType.insert(std::make_pair("key_thief", ObjectManager::THIEF_T));
    _stringToType.insert(std::make_pair("warrior", ObjectManager::WARRIOR_T));
    _stringToType.insert(std::make_pair("wall", ObjectManager::WALL_T));
    _stringToType.insert(std::make_pair("fire", ObjectManager::FIRE_T));
    _stringToType.insert(std::make_pair("open_gate", ObjectManager::GATE_T));
    _stringToType.insert(std::make_pair("closed_gate", ObjectManager::GATE_T));
    _stringToType.insert(std::make_pair("key", ObjectManager::KEY_T));
    _stringToType.insert(std::make_pair("teleport", ObjectManager::TELEPORT_T));
    _stringToType.insert(std::make_pair("orc", ObjectManager::ORC_T));
    _stringToType.insert(std::make_pair("blank", ObjectManager::BLANK_T));
    _stringToType.insert(std::make_pair("throne", ObjectManager::THRONE_T));

    
    cleanForNewLevel();
}

void ObjectManager::initTeleports()
{
    _teleports.clear();
    _teleports.resize(COUPLES_OF_TELEPORTS);
    for (unsigned int i = 0; i < COUPLES_OF_TELEPORTS; ++i)
    {
        _teleports[i].a = nullptr;
        _teleports[i].b = nullptr;
    }
}

void ObjectManager::initCharacters()
{
    _characters.clear();
    _characters.resize(Characters_E::NUM_OF_CHARACTERS, nullptr);

}



ObjectManager* ObjectManager::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new ObjectManager;
    }
    return _instance;
}

ObjectManager::~ObjectManager()
{
}

void ObjectManager::cleanForNewLevel()
{
    initCharacters();
    initTeleports();
}

GameObject* ObjectManager::create(
    const LevelBuilder::ObjectInfo& info, const sf::Vector2u& pos)
{
    Objects_E type = _stringToType[info.name];


    GameObject* o = getObject(type);

    o->setName(info.name);
    if (info.hasColor) {
        o->setColor(info.color);
    }

    o->setBoardPosition(pos);

    if (isCharacter(type))
    {
        handleCharacter(type, o);

        //// return blank tile instead of the character
        BlankTile* b = new BlankTile;
        b->setBoardPosition(info.allPositions[0]);
        return b;
    }

    if (type == Objects_E::TELEPORT_T)
    {
        setTeleportTwin(dynamic_cast<Teleport*>(o));
    }

    return o;
}

void ObjectManager::handleCharacter(
    ObjectManager::Objects_E type, GameObject* object)
{
    switch (type)
    {
        case Objects_E::KING_T:
            _characters[Characters_E::KING_C] = dynamic_cast<King*>(object);
            break;

        case Objects_E::MAGE_T:
            _characters[Characters_E::MAGE_C] = dynamic_cast<Mage*>(object);
            break;

        case Objects_E::THIEF_T:
            _characters[Characters_E::THIEF_C] = dynamic_cast<Thief*>(object);
            break;

        case Objects_E::WARRIOR_T:
            _characters[Characters_E::WARRIOR_C] = dynamic_cast<Warrior*>(object);
            break;

        default:
            break;
    }
}

void ObjectManager::setTeleportTwin(Teleport* t)
{
    for (unsigned int i = 0; i < COUPLES_OF_TELEPORTS; ++i)
    {
        if (_teleports[i].a == nullptr)
        {
            _teleports[i].a = t;
            break;
        }
        else if (_teleports[i].b == nullptr)
        {
            _teleports[i].b = t;
            t->setTwin(_teleports[i].a);
            _teleports[i].a->setTwin(t);
            break;
        }
    }
}


bool ObjectManager::isCharacter(const ObjectManager::Objects_E type) const
{

    return type == ObjectManager::Objects_E::KING_T
        || type == ObjectManager::Objects_E::MAGE_T
        || type == ObjectManager::Objects_E::THIEF_T
        || type == ObjectManager::Objects_E::WARRIOR_T;
}


bool ObjectManager::isCharacter(const string& name) const
{
    ObjectManager::Objects_E type = _stringToType.at(name);
    return isCharacter(type);
}

GameObject* ObjectManager::getObject(ObjectManager::Objects_E type) const
{
    switch (type)
    {
        case ObjectManager::KING_T:
            return new King;
            break;
        case ObjectManager::MAGE_T:
            return new Mage;
            break;
        case ObjectManager::THIEF_T:
            return new Thief;
            break;
        case ObjectManager::WARRIOR_T:
            return new Warrior;
            break;
        case ObjectManager::BLANK_T:
            return new BlankTile;
            break;
        case ObjectManager::WALL_T:
            return new Wall;
            break;
        case ObjectManager::FIRE_T:
            return new Fire;
            break;
        case ObjectManager::THRONE_T:
            return new Throne;
            break;
        case ObjectManager::GATE_T:
            return new Gate;
            break;
        case ObjectManager::KEY_T:
            return new Key;
            break;
        case ObjectManager::ORC_T:
            return new Orc;
            break;
        case ObjectManager::TELEPORT_T:
            return new Teleport;
            break;

        default:
            return nullptr;
            break;
    }
}

vector<Character*> ObjectManager::getCharacters() const
{
    return _characters;
}

