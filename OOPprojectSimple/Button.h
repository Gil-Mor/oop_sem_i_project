#pragma once
#include <SFML\Graphics.hpp>
#include "MouseInteractable.h"

class Button : public MouseInteractable
{
public:

    Button();
    Button(const sf::Vector2f& size, const sf::Vector2f& pos);

    virtual ~Button();

    virtual void action()const=0;

    virtual void setInFocus(bool f);
    virtual void setChosen(bool c);


};



