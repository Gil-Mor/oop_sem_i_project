#pragma once

/* A user controlable Character. 
Base Class of: King, Mage, Thief, and Warrior. */
#include "Movable.h"
#include "CharacterBoardCommand.h"
#include "BlankTile.h"
#include <vector>

class Board;
class CharacterBoardCommand;

using std::vector;

class Character :
    public Movable
{
public:

    Character();
    virtual ~Character()=0;

    void setCommands(CharacterBoardCommand* commands);

    bool move(const sf::Keyboard::Key key);
    bool move(const sf::Vector2u destination);

    // override from MouseInteractable. setColors
    virtual void setInFocus(bool f);
    virtual void setChosen(bool c);

protected:


    CharacterBoardCommand* _commands;
    
    void moveToPosition(const sf::Vector2u& position);

    sf::Vector2u Character::getNextPositionFromKey(
        const sf::Keyboard::Key key) const;


};

