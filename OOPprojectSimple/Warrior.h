#pragma once
#include "Character.h"

class Warrior : public Character

{
public:

    Warrior();
    virtual ~Warrior();

    /* kill the orc in the */
    void kill(Orc& orc);

    virtual void colide(GameObject& other);
    virtual void colide(Warrior& other);
    virtual void colide(King& other);
    virtual void colide(Mage& other);
    virtual void colide(Thief& other);
    virtual void colide(BlankTile& other);
    virtual void colide(Wall& other);
    virtual void colide(Fire& other);
    virtual void colide(Throne& other);
    virtual void colide(Gate& other);
    virtual void colide(Key& other);
    virtual void colide(Orc& other);
    virtual void colide(Teleport& other);

private:

    unsigned int _health;
    unsigned int _bodyCount;
};

