#include "Controller.h"
#include "StateCommands.h"
#include "TextureManager.h"

using std::string;

const string TITLE = "Save The Kingush";
const string FONT_FILE_NAME = "../SourceFont/impact.ttf";

const string LEVELS_DIR = "../levels/";
const string LEVELS_FILE = "../levels/levels.stk";

const string MUSIC_DIR = "../SourceMusic/";
const string WIN_SOUND_FILE = "../SourceSounds/king_in_the_castle.wav";
const string FOOLED_AROUND_SONG = MUSIC_DIR + "fooled_around.ogg";
const string WEIRD_BEARD_SONG = MUSIC_DIR + "Weird_Beard.ogg";
const string RONDO_SONG = MUSIC_DIR + "rondo.ogg";


Controller::Controller()
    :

    // first state is showing the menu
    _state(Controller::State_E::ShowingMenu_S),

    // initialize stateCommands for menu and level
    _stateCommands(*new StateCommands(*this)),
    _level(_stateCommands),
    _menu(_stateCommands, _soundTrack),
    _currLevel(0)
{

    // initialize the main SFML window.
    InitWindow();

    // initilze message window stuff like text, font, etc..
    InitMessageDisplay();

    // need to create them after window is initialized because they need
    // the size..
    _menu.create(_mainWindow);

    // create the level window.. this doesn't set new level
    // just creates the window.
    _level.create(_mainWindow);

    // initalize sounds.
    InitSounds();

    // load new level to play.
    loadLevel();
}


void Controller::loadLevel()
{
    // clean before each new level.
    cleanAfterLevel();

    // if the userplayed all levels
    if (_currLevel >= _levelBuilder.numOfLevels())
    {
        setState(Controller::State_E::FinishedLevels_S);
        return;
    }

    // get level struct from levelBbuilder (LevelManager)
    // holds all information on the current level.
    _levelStruct = _levelBuilder.loadLevel(_currLevel);


    // set the new Level.
    _level.setNewLevel(_levelStruct);
}


Controller::~Controller()
{
    cleanAfterLevel();
}


void Controller::run()
{
    _soundTrack.play();

    // play while we're not exisiting.
    while (getState() != Controller::State_E::Exiting_S)
    {
        switch (getState())
        {
            case Controller::State_E::ShowingMenu_S:
                _menu.run(_mainWindow);
                break;

            case Controller::State_E::Playing_S:
                _level.run(_mainWindow);
                break;

            case Controller::State_E::Winning_S:
                win();               
                break;

            case Controller::State_E::Losing_S:
                lose();
                break;

            case Controller::State_E::FinishedLevels_S:
                finish();
                break;


            default:
                break;
        }
    }

    // this is where we quit the program
    displayMessage("<   goodbye  !   >", 140);
    _soundTrack.stop();
    _mainWindow.close();
}

void Controller::win()
{
    _winSound.play();

    _currLevel++;
    if (_currLevel >= _levelBuilder.numOfLevels())
    {
        setState(Controller::State_E::FinishedLevels_S);
    }
    else
    {
        setState(Controller::State_E::Playing_S);
        loadLevel();
    }
}

void Controller::lose()
{

    cleanAfterLevel();
    displayMessage("YOU'VE LOST.\nYOU CAN TRY THIS LEVEL AGAIN", 60);
    _levelStruct = _levelBuilder.loadLevel(_currLevel);
    _level.setNewLevel(_levelStruct);

    setState(Controller::State_E::ShowingMenu_S);

}

void Controller::finish()
{
    displayMessage("YOUVE FINISHED ALL LEVELS!\n", 70);
    _currLevel = 0;
    loadLevel();
    setState(State_E::ShowingMenu_S);
}

Controller::State_E Controller::getState() const
{
    return _state;
}

void Controller::setState(Controller::State_E state)
{
    _state = state;
}

void Controller::displayMessage(
    const string message, unsigned int fontSize)
{
    _message.text.setString(message);
    _message.text.setCharacterSize(fontSize);

    sf::Vector2f center;

    sf::FloatRect textSize;

    textSize = _message.text.getGlobalBounds();

    // make the center of text it's origin.
    center = { textSize.width / 2.f, textSize.height / 2.f };

    _message.text.setOrigin(center);

    // display background for messages.
    _mainWindow.draw(_message._messageBG);


    _mainWindow.draw(_message.text);

    _mainWindow.display();

    sf::Event event;

    while (true)
    {
        while (_mainWindow.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                case sf::Event::KeyPressed:
                case sf::Event::MouseButtonPressed:
                    return;
            }
        }
    }
}

void Controller::InitWindow()
{ 
    sf::VideoMode fullScreen = sf::VideoMode::getDesktopMode();
    fullScreen.width -= 20;
    fullScreen.height -= 100;

    _mainWindow.create(fullScreen, TITLE);

    // set window position to the top left corner of the screen
    _mainWindow.setPosition({ 0, 0 });

    // set some frame limit.. we don't need much..
    _mainWindow.setFramerateLimit(50);
}

void Controller::InitSounds()
{
    _soundTrack.openFromFile(FOOLED_AROUND_SONG);
    _soundTrack.setVolume(20);
    _soundTrack.setLoop(true);

    _winSoundBuf.loadFromFile(WIN_SOUND_FILE);
    _winSound.setBuffer(_winSoundBuf);
    _winSound.setVolume(50);
}

void Controller::setVolume(const float volume)
{
    _soundTrack.setVolume(volume);
}

float Controller::getVolume() const
{
    return _soundTrack.getVolume();
}

void Controller::InitMessageDisplay()
{
    _message._messageBG.setSize(sf::Vector2f(_mainWindow.getSize()));
    _message._messageBG.setPosition(sf::Vector2f(_mainWindow.getPosition()));
    _message._messageBG.setFillColor(sf::Color::White);
    _message._messageBG.setTexture(TextureManager::getInstance()
        ->getControllerTex(Controller_Tex::MESSAGE_BACKGROUND_T));

    _message.text.setStyle(sf::Text::Regular);

    _message.text.setPosition(sf::Vector2f(_mainWindow.getSize().x / 2.f,
        _mainWindow.getSize().y / 2.f));

    _message.font.loadFromFile(FONT_FILE_NAME);

    _message.text.setFont(_message.font);

    _message.text.setColor(sf::Color::Red);

}

void Controller::cleanAfterLevel()
{
    // clean board.
    for (unsigned int i = 0; i < _levelStruct.board.size(); ++i)
    {
        for (unsigned int j = 0; j < _levelStruct.board[i].size(); ++j)
        {
            delete _levelStruct.board[i][j];
            _levelStruct.board[i][j] = nullptr;
        }
    }

    _levelStruct.board.clear();

    // clean characters.
    for (unsigned int i = 0; i < _levelStruct.characters.size(); ++i)
    {
        delete _levelStruct.characters[i];
        _levelStruct.characters[i] = nullptr;
    }
    _levelStruct.characters.clear();

    // tell object manager that collected charcters and teleports
    // to clean up for next level loading.
    ObjectManager::getInstance()->cleanForNewLevel();


}
