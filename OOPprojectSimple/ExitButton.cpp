#include "ExitButton.h"
#include "StateCommands.h"
#include "TextureManager.h"

ExitButton::ExitButton(const sf::Vector2f& size, 
    const sf::Vector2f pos, StateCommands& commands)
    : Button(size, pos), _commands(commands)
{
    _shape.setTexture(TextureManager::getInstance()->
        getMenuTex(Menu_Tex::EXIT_T));
    _shape.setFillColor(sf::Color::White);
}


ExitButton::~ExitButton()
{}

void ExitButton::action() const
{
    _commands.setState(Controller::State_E::Exiting_S);
}