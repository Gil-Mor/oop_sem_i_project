#pragma once

/* Singleton incharge of loading and keeping
all the program textures. */

#include <SFML\Graphics.hpp>
#include <vector>
#include <string>

using std::vector;
using std::string;


// each inside a namespace because then i
// can has same names like backgroud_t...
namespace Controller_Tex
{
    enum Controller_E
    {
        MESSAGE_BACKGROUND_T = 0
    };
}

namespace Board_Tex
{
    enum Board_E
    {
        BACKGROUND_T = 0
    };
}
namespace Objects_Tex
{
    enum Objects_E
    {
        KING_T = 0,
        MAGE_T,
        NO_KEY_THIEF_T,
        KEY_THIEF_T,
        WARRIOR_T,

        BLANK_T,
        WALL_T,
        FIRE_T,
        THRONE_T,
        CLOSED_GATE_T,
        OPEN_GATE_T,
        KEY_T,
        ORC_T,
        TELEPORT_T,


        NUM_OF_OBJECTS_T
    };
}

namespace Menu_Tex
{
    enum Menu_E
    {
        BACKGROUND_T = 0,

        PLAY_T,
        EXIT_T,
        VOLUME_UP_T,
        VOLUME_DOWN_T,
        NUM_OF_TEXS_T
    };
}

class TextureManager
{
public:

    static TextureManager* getInstance();

    const sf::Texture* getObjectsTex(Objects_Tex::Objects_E) const;
    const sf::Texture* getMenuTex(Menu_Tex::Menu_E) const;
    const sf::Texture* getControllerTex(Controller_Tex::Controller_E) const;
    const sf::Texture* getBoardTex(Board_Tex::Board_E) const;

private:

   static TextureManager* _instance;

    TextureManager();

    ~TextureManager();

    void loadTextures(vector<const sf::Texture*>& v,
        const string fileName);

    // saving textures in vectors even if it's only one texture
    // gives more flexability.
    vector<const sf::Texture*> _objectsTextures;
    vector<const sf::Texture*> _boardTextures;
    vector<const sf::Texture*> _menuTextures;
    vector<const sf::Texture*> _controllerTextures;
    

    // make copy cotr ad assignment operator private.
    TextureManager(const TextureManager&);

    TextureManager& operator=(const TextureManager&);

};

