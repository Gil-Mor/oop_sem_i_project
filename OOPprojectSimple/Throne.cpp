#include "Throne.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Throne::Throne()
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::THRONE_T));
}


Throne::~Throne()
{
}

void Throne::colide(GameObject& other)
{
    other.colide(*this);
}
void Throne::colide(Mage& other)
{
    other.colide(*this);
}

void Throne::colide(King& other)
{
    other.colide(*this);
}

void Throne::colide(Thief& thief)
{
    thief.colide(*this);
}

void Throne::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
