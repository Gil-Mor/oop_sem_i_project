#pragma once
/* Encapsulate calls to member function of Board class giving 
only needed funcionality to objects that need it. */
class Board;
class BoardCommand
{
public:
    BoardCommand(Board& board);
    virtual ~BoardCommand()=0;

protected:
    Board& _board;
};

