/* Gives the characters access to functions they need
from the board. sort of Command or Mediator patterns?..

This is the way i chose to give the characters only 
funcionality they need and not the whole public 
functions of the board if i'd passed the Board as a reference.
*/

#pragma once
#include "BoardCommand.h"
#include <SFML\Graphics.hpp>

class Board;
class GameObject;

class CharacterBoardCommand :
    public BoardCommand
{
public:
    CharacterBoardCommand(Board& board);
    ~CharacterBoardCommand();

    bool validPosition(const sf::Vector2u& pos) const;

    GameObject* getFromBoard(const sf::Vector2u& pos) const;

    void setOnBoard(GameObject* obj, const sf::Vector2u& pos);

    sf::Vector2f getTileSFMLPosition(const sf::Vector2u pos) const;

    bool thiefHasKey() const;

};

