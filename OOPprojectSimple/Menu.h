#pragma once
#include "Button.h"
#include <vector>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "Drawable.h"

using std::vector;

class Controller;

class StateCommands;
//class VolumeCommands;

class Menu : public mydrawable::Drawable
{
public:

    Menu(StateCommands& stateCommands, sf::Music& _soundTrack);

    // Need the window for size and position.
    void create( sf::RenderWindow& window);

    // maybe there'll be derived menus...
    virtual ~Menu();

    /* Iterate through the buttons and activate action
    In the button that was clicked.*/
    void run(sf::RenderWindow& window);

    void handleEvent(const sf::RenderWindow& window, 
        const sf::Event event);

    void show(sf::RenderWindow& window) const;

    enum Buttons_E
    {
        Play_B = 0,
        Exit_B,
        VOLUME_UP_B,
        VOLUME_DOWN_B
    };
    
private:

    /* vectorof menu buttons. */
    vector<Button*> _buttons;
    sf::Vector2f _center;

    StateCommands& _stateCommands;

    sf::Music& _soundTrack;

    Button* _chosenButton;


    void setButtons();

    void handleMouseMove(const sf::Vector2f mouse);
    void handleMouseDrag(const sf::Vector2f mouse);
    void handleMousePress(const sf::Vector2f mouse);
    void handleMouseRelease(const sf::Vector2f mouse);

    void cleanMouse();
    bool buttonWasPressed() const;

    void setPlayButton();
    void setExitButton();
    void setVolumeUpButton();
    void setVolumeDownButton();

};


