#include "BlankTile.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

BlankTile::BlankTile()
{
    //_shape.setTexture(
    //    TextureManager::getInstance()->getObjectsTex()[Objects_Tex::BLANK_T]);
    //_shape.setFillColor({ 255, 255, 255, 50 });
    _shape.setFillColor(sf::Color::Transparent);
    _shape.setOutlineThickness(0);
    _shape.setOutlineColor(sf::Color::Black);
}


BlankTile::~BlankTile()
{
}

void BlankTile::colide(GameObject& other)
{
    other.colide(*this);
}

void BlankTile::colide(Mage& mage)
{
    mage.colide(*this);
}

void BlankTile::colide(King& king)
{
    king.colide(*this);
}

void BlankTile::colide(Thief& thief)
{
    thief.colide(*this);
}

void BlankTile::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
