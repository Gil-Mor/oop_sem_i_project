#pragma once
#include "TextureManager.h"
#include "MouseInteractable.h"

class Board;

class King;
class Mage;
class Thief;
class Warrior;
class BlankTile;
class Wall;
class Fire;
class Throne;
class Gate;
class Key;
class Orc;
class Teleport;

#include <string>
using std::string;

// SIZE OF ALL GAME OBJECTS
const sf::Vector2f OBJECT_SIZE(50, 50);

class GameObject : public MouseInteractable
{
public:

    GameObject();
    virtual ~GameObject()=0;

    /* Get the object texture.
    Used by the side bar Menu to display the 
    Texture of the current character.*/

    const sf::Texture* getTexture() const;


    sf::Vector2u getBoardPosition() const;
    void setBoardPosition(const sf::Vector2u pos);
    //void swapPositions(GameObject& other);

    sf::Vector2f getShapePosition() const;
    void setShapePosition(const sf::Vector2f pos);

    void setColor(const sf::Color& color);

    string getName() const;
    void setName(const string& name);


    virtual void colide(GameObject& other)=0;
    virtual void colide(King& other)=0;
    virtual void colide(Mage& other)=0;
    virtual void colide(Thief& other)=0;
    virtual void colide(Warrior& other)=0;
    //virtual void colide(BlankTile& other)=0;
    //virtual void colide(Wall& other)=0;
    //virtual void colide(Fire& other);
    //virtual void colide(Throne& other);
    //virtual void colide(Gate& other);
    //virtual void colide(Key& other);
    //virtual void colide(Orc& other);
    //virtual void colide(Teleport& other);

private:

    /* The object position on the board. */
    sf::Vector2u _position;

    string _name;

};

