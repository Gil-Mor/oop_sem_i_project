#include "Teleport.h"
#include "Mage.h"
#include "King.h"
#include "Thief.h"
#include "Warrior.h"

Teleport::Teleport()
    :_twin(nullptr)
{
    _shape.setTexture(
        TextureManager::getInstance()->getObjectsTex(Objects_Tex::TELEPORT_T));
}


Teleport::~Teleport()
{
}

void Teleport::setTwin(const Teleport* twin)
{
    _twin = twin;
}


sf::Vector2u Teleport::getTwinPosition() const
{
    return _twin->getBoardPosition();
}

void Teleport::colide(GameObject& other)
{
    other.colide(*this);
}
void Teleport::colide(Mage& other)
{
    other.colide(*this);
}

void Teleport::colide(King& king)
{
    king.colide(*this);
}

void Teleport::colide(Thief& thief)
{
    thief.colide(*this);
}

void Teleport::colide(Warrior& warrior)
{
    warrior.colide(*this);
}
