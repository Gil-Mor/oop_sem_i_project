#pragma once
#include "GameObject.h"
class Orc :
    public GameObject
{
public:
    Orc();
    ~Orc();

    virtual void colide(GameObject& other);
    virtual void colide(Mage& other);
    virtual void colide(King& other);
    virtual void colide(Thief& other);
    virtual void colide(Warrior& other);

};

